Drupal API Key Authentication Provider for REST Resources
================

Drupal 8 Authentication Provider using an API Key as GET or POST Parameters.

based on https://github.com/enzolutions/ip_consumer_auth
